#!/usr/bin/env python

import flask, yaml
from flask_frozen import Freezer
import sys, getopt

def usage():
  print("arlo_flask.py ( --thawed | --frozen)")


# Create the application.
APP = flask.Flask(__name__)
APP.debug = False
APP.testing = True


###
# Routes

@APP.route('/')
def home():
  return flask.render_template('index.html')

@APP.route('/contributing/')
def contributing():
  return flask.render_template('contributing.html')



def main(argv):

  ###
  # Get Command Line Options

  # Check if we are running a 'frozen' build or thawed. 
  run_thawed = None
  
  try:
    opts, args = getopt.getopt(argv,"tf",["thawed","frozen"])
  except getopt.GetoptError:
    usage()
    sys.exit(2)

  for opt, arg in opts:
    if opt in ("-t", "--thawed"):
      run_thawed = True
    elif opt in ("-f", "--frozen"):
      run_thawed = False

  if run_thawed is None:
    usage()
    sys.exit(3)

  ###
  # Run... 

  if run_thawed:
    APP.debug=True
    APP.run(host='0.0.0.0')
  else:
    freezer= Freezer(APP)
    freezer.freeze()


if __name__ == '__main__':
    main(sys.argv[1:])

