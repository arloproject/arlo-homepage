ARLO Project - Static Homepage
============

This repo builds the static homepage for http://arloproject.com

This is a Flask app that compiles down into static html, which is then deployed to the webserver. 


### Required Modules

Python requirements are maintained in requirements.txt. 

To install with pip: 
`pip install -r requirements.txt`


### Developing and Building the Site

I recommend first building a virtualenv for the app, and installing the necessary modules: 
```
virtualenv arlo-homepage-virtualenv
source arlo-homepage-virtualenv/bin/activate
cd arlo-homepage
pip install -r requirements.txt
```

The arlo\_flask.py script has two modes of operation. 
* --thawed runs a development server, by default on localhost port 5000
* --frozen builds the entire site into build/

### License

ARLO is released under the University of Illinois/NCSA Open Source License. 

See the LICENSE file in the root of this repository for further details.